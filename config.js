module.exports = {
    platform: 'gitlab',
    endpoint: process.env.CI_API_V4_URL,
    token: process.env.RENOVATE_TOKEN,
    onboarding: true,
    onboardingConfig: {
        extends: [
            'config:base',
            'docker:enableMajor',
            ':separateMultipleMajorReleases',
            ':prHourlyLimitNone'

        ],
        branchPrefix: "chore/renovabot-",
        branchNameStrict: true,
        commitMessagePrefix: "chore(renovabot): ",
        commitMessageLowerCase: "auto"
    },
    repositories: process.env.REPOSITORIES.split(',')
};
